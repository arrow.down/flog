# 简介

FLog 是一个轻量简洁的日志库。使用方式完全可以替换Android原生Log。

实现开箱即用，无需复杂的配置。

用户可以参考自行实现自己的ILog,比如用来做bug收集上报之类的集成

[![Maven Central](https://img.shields.io/maven-central/v/cn.wangzq/flog.svg?label=Maven%20Central)](https://central.sonatype.com/artifact/cn.wangzq/flog/)
[![Latest Release](https://gitlab.com/arrow.down/flog/-/badges/release.svg)](https://gitlab.com/arrow.down/flog/-/releases)

## 使用方式

### 集成引用

```groovy

dependencies {
    implementation 'cn.wangzq:flog:lastRelease'
    implementation 'cn.wangzq:flog-lib:lastRelease'
}

```

### 使用说明

```java
//可直接全文搜索替换  import android.util.Log;

import cn.wangzq.flog.Log;


        //初始化配置示例 可参考 example 
        Properties properties=new Properties();
        try(InputStream stream=getAssets().open("flog/config.properties")){
            properties.load(stream);
            Log.init(properties);
        }catch(Exception e){
            e.printStackTrace();
        }

// 如果以上服务不能满足业务需求，可以自行继承ILog 并添加自己的实现


// 和 android log  一致的 api
        Log.v("VVVVVVV","asadasdasdasd ---- ");
        Log.d("DDDDDDD","asadasdasdasd ---- ");
        Log.i("IIIIIII","asadasdasdasd ---- ");
        Log.w("WWWWWWW","asadasdasdasd ---- ");
        Log.w("WWW",new Error("xadsadfhlakshdflkjahsdkjfh"));
        Log.w("WWW","wwww",new Error("xadsadfhlakshdflkjahsdkjfh"));
        Log.e("EEEEEEE","asadasdasdasd ---- ");
        Log.e("EEE","eeee",new Error("xadsadfhlakshdflkjahsdkjfh"));


```

## 配置文件说明

### LogcatAppender支持配置

| 配置名   | 参数类型   | 默认值     | 说明                                              |
|-------|--------|---------|-------------------------------------------------|
| Level | String | VERBOSE | log打印级别 ,支持: VERBOSE,DEBUG,INFO,WARN,ERROR,NONE |
| Tag   | String | FLOG    | log  tag                                        |

### RollingFileAppender 支持配置

| 配置名           | 参数类型    | 默认值                                               | 说明                                              |
|---------------|---------|---------------------------------------------------|-------------------------------------------------|
| MaxFileCount  | int     | 10                                                | log 文件最大记录数量                                    |
| MaxFileSize   | long    | 5242880 （5M）                                      | log 单文件大小 单位 byte                               |
| Path          | String  | 无                                                 | log文件的保存目录                                      |
| Prefix        | String  | flog                                              | log 文件的前缀                                       |
| Gzip          | boolean | true                                              | log 文件是否开启压缩。开启后非当前log 会压缩为gz 文件减小空间占用，         |
| Level         | String  | VERBOSE                                           | log打印级别 ,支持: VERBOSE,DEBUG,INFO,WARN,ERROR,NONE |
| FilterMessage | boolean | false                                             | log 文件消息是否开启长度压缩过滤器。开启超过128的重复消息会被压缩            |
| Secret        | boolean | false                                             | log 内容是否加密                                      |
| KeyGenerate   | String  | cn.wangzq.flog.appender.secret.DefaultKeyGenerate | log 加密key 生成类                                   |

### FixedFileAppender 支持配置

| 配置名           | 参数类型    | 默认值                                               | 说明                                              |
|---------------|---------|---------------------------------------------------|-------------------------------------------------|
| MaxFileSize   | long    | 52428800 （50M）                                    | log 文件大小（固定大小占位） 单位 byte                        |
| Path          | String  | 无                                                 | log文件的保存位置                                      |
| Level         | String  | VERBOSE                                           | log打印级别 ,支持: VERBOSE,DEBUG,INFO,WARN,ERROR,NONE |
| FilterMessage | boolean | false                                             | log 文件消息是否开启长度压缩过滤器。开启超过128的重复消息会被压缩            |
| Secret        | boolean | false                                             | log 内容是否加密                                      |
| KeyGenerate   | String  | cn.wangzq.flog.appender.secret.DefaultKeyGenerate | log 加密key 生成类                                   |

**注意: FixedFileAppender 目前不支持多进程日志记录**

### 配置文件示例

```properties
# 配置文件参考
# 配置开启的Log服务,可以按照不同需求配置不同的输出
flog.Logger=Console,RollingFile,FixedFile
# 默认配置log 打印级别
flog.Level=VERBOSE
# Console 配置 指定 appender为logcat 输出实现
flog.appender.Console=cn.wangzq.flog.appender.LogcatAppender
# 可以独立配置log 输出级别
flog.appender.Console.Level=V
# 默认TAG 
flog.appender.Console.Tag=FLOG
# FixedFile  固定文件大小记录
flog.appender.FixedFile=cn.wangzq.flog.appender.FixedFileAppender
flog.appender.FixedFile.Path=/data/user/0/cn.wangzq.flog.example/cache/fixed.log
flog.appender.FixedFile.MaxFileSize=5242880
flog.appender.FixedFile.Level=V
flog.appender.FixedFile.FilterMessage=false
flog.appender.FixedFile.Secret=false
flog.appender.FixedFile.KeyGenerate=cn.wangzq.flog.appender.secret.DefaultKeyGenerate
# RollingFile 滚动文件记录
flog.appender.RollingFile=cn.wangzq.flog.appender.RollingFileAppender
flog.appender.RollingFile.Path=/data/user/0/cn.wangzq.flog.example/cache/logs
flog.appender.RollingFile.MaxFileSize=5242880
flog.appender.RollingFile.Level=V
flog.appender.RollingFile.FilterMessage=false
flog.appender.RollingFile.Secret=false
flog.appender.RollingFile.KeyGenerate=cn.wangzq.flog.appender.secret.DefaultKeyGenerate
flog.appender.RollingFile.MaxFileCount=10
flog.appender.RollingFile.Prefix=example
flog.appender.RollingFile.Gzip=true


```
