package cn.wangzq.flog;


import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class LogTest {

    String TAG = "TEST";

    @Test
    public void initAll() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);

        Field field = Log.class.getDeclaredField("sLogger");
        field.setAccessible(true);
        HashMap<String, ILog> map = (HashMap) field.get(null);
        Assert.assertEquals(3, map.size());

        List<String> keys = List.of(
                "flog.appender.Console",
                "flog.appender.RollingFile",
                "flog.appender.FixedFile"
        );

        Assert.assertTrue(map.keySet().containsAll(keys));

    }

    @Test
    public void initWithError() throws Exception {

        boolean hasError = false;
        try {
            Properties properties = new Properties();
            Log.init(properties);
        } catch (IllegalArgumentException e) {
            hasError = true;
        }

        Assert.assertTrue(hasError);

        hasError = false;
        try {
            Properties properties = new Properties();
            properties.put("flog.Logger", "xx");
            Log.init(properties);
        } catch (NullPointerException e) {
            hasError = true;
        }

        Assert.assertTrue(hasError);

    }

    @Test
    public void v() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void v(String tag, String msg)";
        Log.v(TAG, msg);
    }

    @Test
    public void v1() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void v(String tag, String msg, Throwable e)";
        Log.v(TAG, msg, new Throwable("test Throwable"));
    }

    @Test
    public void d() throws Exception {
        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void d(String tag, String msg)";
        Log.d(TAG, msg);

    }

    @Test
    public void i() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void i(String tag, String msg)";
        Log.i(TAG, msg);
    }

    @Test
    public void w() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void w(String tag, String msg)";
        Log.w(TAG, msg);
    }

    @Test
    public void w1() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void w(String tag, String msg , Throwable e)";
        Log.w(TAG, msg, new Throwable("test Throwable"));
    }

    @Test
    public void w2() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void w(String tag, Throwable e)";
        Log.w(TAG, new Throwable("test Throwable"));
    }

    @Test
    public void e() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void w(String tag, String msg)";
        Log.e(TAG, msg);
    }

    @Test
    public void e1() throws Exception {

        Properties properties = new Properties();
        String path = "all.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(path);) {
            properties.load(stream);
        }

        Log.init(properties);
        String msg = "test with method :" + " void w(String tag, String msg , Throwable e)";
        Log.e(TAG, msg, new Throwable("test Throwable"));
    }
}