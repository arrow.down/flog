package cn.wangzq.flog.appender;


import static cn.wangzq.flog.appender.BaseFileAppender.KEY_FILTER_ENABLE;
import static cn.wangzq.flog.appender.BaseFileAppender.KEY_LOG_PATH;
import static cn.wangzq.flog.appender.BaseFileAppender.KEY_MAX_FILE_SIZE;
import static cn.wangzq.flog.appender.BaseFileAppender.KEY_SECRET;
import static cn.wangzq.flog.appender.RollingFileAppender.*;

import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

public class RollingFileAppenderTest {


    @Test
    public void init() {
        RollingFileAppender appender = new RollingFileAppender();

        long maxFileSize = 1024 * 1024 * 2L;
        String logPath = System.getProperty("java.io.tmpdir") + "/rolling";
        boolean filter = true;
        boolean secret = true;
        int maxFileCount = 20;
        String prefix = "test_rolling";
        boolean gz = false;


        Properties properties = new Properties();
        properties.put(KEY_MAX_FILE_SIZE, "" + maxFileSize);
        properties.put(KEY_LOG_PATH, logPath);
        properties.put(KEY_FILTER_ENABLE, "" + filter);
        properties.put(KEY_SECRET, "" + secret);
//        properties.put(KEY_SECRET_KEY_GENERATE,"");
        properties.put(KEY_MAX_FILE_COUNT, "" + maxFileCount);
        properties.put(KEY_FILE_PREFIX, prefix);
        properties.put(KEY_GZ_ENABLE, "" + gz);

        appender.init(properties);

        Assert.assertEquals(maxFileSize, appender.getMaxFileSize());
        Assert.assertEquals(logPath, appender.getLogPath());
        Assert.assertEquals(filter, appender.isFilterMsg());
        Assert.assertEquals(secret, appender.isSecret());

        Assert.assertEquals(maxFileCount, appender.getMaxFileCount());
        Assert.assertEquals(prefix, appender.getFilePrefix());
        Assert.assertEquals(gz, appender.isGzEnable());

    }

    @Test
    public void writeToFile() {


        RollingFileAppender appender = new RollingFileAppender();

        long maxFileSize = 1024 * 8L;
        String logPath = System.getProperty("java.io.tmpdir") + "/rolling";
        boolean filter = true;
        boolean secret = true;
        int maxFileCount = 4;
        String prefix = "test_rolling";
        boolean gz = true;


        Properties properties = new Properties();
        properties.put(KEY_MAX_FILE_SIZE, "" + maxFileSize);
        properties.put(KEY_LOG_PATH, logPath);
        properties.put(KEY_FILTER_ENABLE, "" + filter);
        properties.put(KEY_SECRET, "" + secret);
//        properties.put(KEY_SECRET_KEY_GENERATE,"");
        properties.put(KEY_MAX_FILE_COUNT, "" + maxFileCount);
        properties.put(KEY_FILE_PREFIX, prefix);
        properties.put(KEY_GZ_ENABLE, "" + gz);

        appender.init(properties);

        Assert.assertEquals(maxFileSize, appender.getMaxFileSize());
        Assert.assertEquals(logPath, appender.getLogPath());
        Assert.assertEquals(filter, appender.isFilterMsg());
        Assert.assertEquals(secret, appender.isSecret());

        Assert.assertEquals(maxFileCount, appender.getMaxFileCount());
        Assert.assertEquals(prefix, appender.getFilePrefix());
        Assert.assertEquals(gz, appender.isGzEnable());

        String msg = " test RollingFileAppender  msg writeToFile !\r\n";

        for (int i = 0; i < 2000; i++) {
            appender.writeToFile(("index:" + i+msg).getBytes());
        }

    }

    @Test
    public void v() throws Exception{
        RollingFileAppender appender = new RollingFileAppender();

        long maxFileSize = 1024 * 8L;
        String logPath = System.getProperty("java.io.tmpdir") + "/rolling";
        boolean filter = true;
        boolean secret = true;
        int maxFileCount = 4;
        String prefix = "test_rolling";
        boolean gz = true;


        Properties properties = new Properties();
        properties.put(KEY_MAX_FILE_SIZE, "" + maxFileSize);
        properties.put(KEY_LOG_PATH, logPath);
        properties.put(KEY_FILTER_ENABLE, "" + filter);
        properties.put(KEY_SECRET, "" + secret);
//        properties.put(KEY_SECRET_KEY_GENERATE,"");
        properties.put(KEY_MAX_FILE_COUNT, "" + maxFileCount);
        properties.put(KEY_FILE_PREFIX, prefix);
        properties.put(KEY_GZ_ENABLE, "" + gz);

        appender.init(properties);

        Assert.assertEquals(maxFileSize, appender.getMaxFileSize());
        Assert.assertEquals(logPath, appender.getLogPath());
        Assert.assertEquals(filter, appender.isFilterMsg());
        Assert.assertEquals(secret, appender.isSecret());

        Assert.assertEquals(maxFileCount, appender.getMaxFileCount());
        Assert.assertEquals(prefix, appender.getFilePrefix());
        Assert.assertEquals(gz, appender.isGzEnable());

        String msg = "test RollingFileAppender  msg writeToFile !";
        for (int i = 0; i < 50; i++) {
            appender.v("TEST","index:"+i+":"+ msg);
            Thread.sleep(10);
        }
    }



    @Test
    public void getMaxFileCount() {

    }

    @Test
    public void getFilePrefix() {
    }

    @Test
    public void isGzEnable() {
    }
}