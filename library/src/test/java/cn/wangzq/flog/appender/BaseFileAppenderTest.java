package cn.wangzq.flog.appender;


import static cn.wangzq.flog.appender.BaseFileAppender.*;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Properties;

public class BaseFileAppenderTest {


    BaseFileAppender appender = new BaseFileAppender() {
        @Override
        void writeToFile(byte[] data) {
            System.out.println("writeToFile:" + Arrays.toString(data));
        }
    };


    @Test
    public void init() {

        long maxFileSize = 1024 * 1024 * 2L;
        String logPath = System.getProperty("java.io.tmpdir");
        boolean filter = true;
        boolean secret = true;


        Properties properties = new Properties();
        properties.put(KEY_MAX_FILE_SIZE, "" + maxFileSize);
        properties.put(KEY_LOG_PATH, logPath);
        properties.put(KEY_FILTER_ENABLE, "" + filter);
        properties.put(KEY_SECRET, "" + secret);
//        properties.put(KEY_SECRET_KEY_GENERATE,"");

        appender.init(properties);

        Assert.assertEquals(maxFileSize, appender.getMaxFileSize());
        Assert.assertEquals(logPath, appender.getLogPath());
        Assert.assertEquals(filter, appender.isFilterMsg());
        Assert.assertEquals(secret, appender.isSecret());


    }

    @Test
    public void v() {
    }

    @Test
    public void testV() {
    }

    @Test
    public void d() {
    }

    @Test
    public void i() {
    }

    @Test
    public void w() {
    }

    @Test
    public void testW() {
    }

    @Test
    public void testW1() {
    }

    @Test
    public void e() {
    }

    @Test
    public void testE() {
    }


    @Test
    public void isFilterMsg() {
    }

    @Test
    public void getLogPath() {
    }

    @Test
    public void getMaxFileSize() {
    }

    @Test
    public void isSecret() {
    }
}