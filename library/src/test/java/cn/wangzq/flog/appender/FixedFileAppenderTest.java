package cn.wangzq.flog.appender;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.Properties;


public class FixedFileAppenderTest {

    FixedFileAppender mAppender = new FixedFileAppender();
    long maxFileSize = 1024 * 5;
    String tempDir = System.getProperty("java.io.tmpdir");
    Properties mockP = new Properties();
    private FileChannel channel;


    @Before
    public void setUp() throws Exception {
        String path = tempDir + File.separator + "FixedFileAppenderTest." + System.currentTimeMillis() + ".log";

        mockP.setProperty("Path", path);
        mockP.setProperty("MaxFileSize", maxFileSize + "");
        mockP.setProperty("Level", "DEBUG");

        mockP.setProperty("Secret", "true");
//        mockP.setProperty("KeyGenerate", "true");
    }

    @Test
    public void init() throws Exception {

        mAppender.init(mockP);

        String path = mAppender.getLogPath();

        long size = mAppender.getMaxFileSize();

        Assert.assertEquals(path, mockP.getProperty("Path"));
        Assert.assertEquals(size + "", mockP.getProperty("MaxFileSize"));

    }

    @Test
    public void initError() throws Exception {

        Properties propError = new Properties();

        boolean hasError = false;
        try {
            mAppender.init(propError);
        } catch (IllegalArgumentException e) {
            hasError = true;
        }
        Assert.assertTrue(hasError);
    }

    @Test
    public void writeToFileZ() throws Exception {
        mAppender.init(mockP);
        mAppender.writeToFile("TEST".getBytes());
    }

    @Test
    public void writeToFile() throws Exception {

        mAppender.init(mockP);
        String msg = "line--------------------------";
        long pos = FixedFileAppender.PROTOCOL_LENGTH;
        while (pos < maxFileSize) {
            String writeMsg = msg + pos + ":" + Long.toString(pos, 16) + "\r\n";

            int len = writeMsg.getBytes(StandardCharsets.UTF_8).length;

            mAppender.writeToFile(writeMsg.getBytes());

            RandomAccessFile file = new RandomAccessFile(mockP.getProperty("Path"), "r");

            channel = file.getChannel();

            ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            channel.position(FixedFileAppender.INDEX_LAST_POS).read(buffer);
            buffer.flip();
            long readPos = buffer.getLong();

            pos += len;
            boolean needBreak = false;
            if (pos >= maxFileSize) {
                pos = FixedFileAppender.PROTOCOL_LENGTH + len;
                needBreak = true;
            }

            System.out.println("nest pos:" + pos + " = " + Long.toString(pos, 16));
            Assert.assertEquals(readPos, pos);
            if (needBreak) break;
        }
    }

    @Test
    public void writeToFileError() {
        Properties propError = new Properties();
        propError.setProperty("Path", tempDir + "/" + Math.random() + "/xx.log");

        mAppender.init(propError);
        boolean hasError = false;
        try {
            mAppender.writeToFile("writeToFileError".getBytes());
        } catch (Exception e) {
            hasError = true;
        }

        Assert.assertTrue(hasError);
    }

    @Test
    public void log() throws InterruptedException {

        mAppender.init(mockP);

        String tag = "Test";
        String msg = "Test Msgxxxxxxxxxxxxxxxx!";
        Throwable throwable = new Throwable("Test Throwable!");
        for (int i = 0; i < 10; i++) {
            mAppender.v(tag, "index:" + i + msg);
        }
        mAppender.v(tag, msg);
        mAppender.v(tag, msg, throwable);
        mAppender.d(tag, msg);
        mAppender.i(tag, msg);
        mAppender.w(tag, msg);
        mAppender.w(tag, throwable);
        mAppender.w(tag, msg, throwable);
        mAppender.e(tag, msg);
        mAppender.e(tag, msg, throwable);
        Thread.sleep(100);

    }

}