package cn.wangzq.flog;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

public class LevelTest {


    HashMap<String, Integer> mParseData = new HashMap<>();

    @Before
    public void setUp() throws Exception {

        mParseData.put("v", Level.VERBOSE);
        mParseData.put("V", Level.VERBOSE);
        mParseData.put("verbose", Level.VERBOSE);
        mParseData.put("VERBOSE", Level.VERBOSE);

        mParseData.put("d", Level.DEBUG);
        mParseData.put("D", Level.DEBUG);
        mParseData.put("debug", Level.DEBUG);
        mParseData.put("DEBUG", Level.DEBUG);

        mParseData.put("i", Level.INFO);
        mParseData.put("I", Level.INFO);
        mParseData.put("info", Level.INFO);
        mParseData.put("INFO", Level.INFO);

        mParseData.put("w", Level.WARN);
        mParseData.put("W", Level.WARN);
        mParseData.put("warn", Level.WARN);
        mParseData.put("WARN", Level.WARN);

        mParseData.put("e", Level.ERROR);
        mParseData.put("E", Level.ERROR);
        mParseData.put("error", Level.ERROR);
        mParseData.put("ERROR", Level.ERROR);

        mParseData.put("CC", Level.NONE);

    }


    @Test
    public void parse() throws Exception{
        mParseData.forEach((k, v) -> Assert.assertEquals(v.intValue(), Level.parse(k)));
    }
}