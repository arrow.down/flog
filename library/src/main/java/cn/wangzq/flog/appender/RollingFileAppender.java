package cn.wangzq.flog.appender;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class RollingFileAppender extends BaseFileAppender {

    protected static final String KEY_MAX_FILE_COUNT = "MaxFileCount";
    protected static final String KEY_FILE_PREFIX = "Prefix";
    protected static final String KEY_GZ_ENABLE = "Gzip";


    private int mMaxFileCount = 10;
    private String mFilePrefix = "flog";
    private boolean gzEnable = false;

    private final ExecutorService mZipThread = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), r -> new Thread(r, "FLOG_ZIP"));


    @Override
    void writeToFile(byte[] data) {
        if (null == getLogPath()) return;
        try (FileOutputStream fos = new FileOutputStream(getLogFile(), true); BufferedOutputStream out = new BufferedOutputStream(fos)) {
            out.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private synchronized void zipLogs() {
        try {
            if (!isGzEnable()) return;
            File[] allLog = new File(getLogPath()).listFiles(mLogFileFilter);
            if (allLog == null || allLog.length <= 1) {
                return;
            }
            sortLogFileList(allLog);

            for (int i = 0; i < allLog.length - 1; i++) {
                File source = allLog[i];
                if (!source.exists()) {
                    continue;
                }
                String name = source.getName();
                if (!name.endsWith(".log")) {
                    continue;
                }

                File target = new File(source.getParentFile(), name.replace(".log", ".zip"));
                try (FileInputStream inputStream = new FileInputStream(source); ZipOutputStream o = new ZipOutputStream(new FileOutputStream(target));) {
                    ZipEntry entry = new ZipEntry(source.getName());
                    o.putNextEntry(entry);
                    int len;
                    byte[] buffer = new byte[4096];
                    while ((len = inputStream.read(buffer)) != -1) {
                        o.write(buffer, 0, len);
                    }
                    o.flush();
                } catch (Exception e) {
                    w("zip file error!", e);
                }
                source.delete();
            }
        } finally {
            removeOldLogs();
        }
    }


    private boolean isOversize(File file) {
        if (null == file) return false;
        boolean res = file.length() > getMaxFileSize();
        if (res) {
            mZipThread.execute(this::zipLogs);
        }
        return res;
    }


    private String getFormatFileName(BigInteger index) {
        return String.format(Locale.getDefault(), "%s.%04d.log", getFilePrefix(), index);
    }

    private synchronized void removeOldLogs() {
        File[] allLog = new File(getLogPath()).listFiles(mLogFileFilter);
        if (allLog == null || allLog.length <= getMaxFileCount()) {
            return;
        }
        sortLogFileList(allLog);
        for (int i = 0; i < allLog.length - getMaxFileCount(); i++) {
            //noinspection ResultOfMethodCallIgnored
            allLog[i].delete();
        }

    }


    private File sLastFile;

    private File getLastLogFile(String path, BigInteger index) {

        File logFile = new File(path, getFormatFileName(index));
        if (logFile.exists() && isOversize(logFile)) {
            index = index.add(BigInteger.valueOf(1));
            logFile = getLastLogFile(path, index);
        }
        return logFile;
    }

    private File getLogFile() {
        File dir = new File(getLogPath());
        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (null == sLastFile || !sLastFile.exists() || isOversize(sLastFile)) {
            File[] oldLogs = dir.listFiles(mLogFileFilter);
            String last = "0";
            if (oldLogs != null && oldLogs.length > 0) {
                sortLogFileList(oldLogs);
                String fileName = oldLogs[oldLogs.length - 1].getName();
                String[] sp = fileName.split("\\.");
                last = sp[sp.length - 2];
            }
            BigInteger index = new BigInteger(last);
            sLastFile = getLastLogFile(getLogPath(), index);
        }
        return sLastFile;
    }

    private final FileFilter mLogFileFilter = pathname -> {
        String fileName = pathname.getName();
        boolean res = fileName.startsWith(getFilePrefix());
        res = res && (fileName.endsWith(".log") || fileName.endsWith(".zip"));
        return res;
    };

    private void sortLogFileList(File[] logs) {
        Arrays.sort(logs, (o1, o2) -> {
            int res = 0;
            try {
                String[] sp1 = o1.getName().split("\\.");
                String[] sp2 = o2.getName().split("\\.");
                String index1 = sp1[sp1.length - 2];
                String index2 = sp2[sp2.length - 2];
                res = new BigInteger(index1).subtract(new BigInteger(index2)).intValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        });
    }


    @Override
    public void init(Properties p) {
        super.init(p);
        mMaxFileCount = Integer.parseInt(p.getProperty(KEY_MAX_FILE_COUNT, mMaxFileCount + ""));
        mFilePrefix = p.getProperty(KEY_FILE_PREFIX, mFilePrefix);
        gzEnable = Boolean.parseBoolean(p.getProperty(KEY_GZ_ENABLE, "true"));
    }

    public int getMaxFileCount() {
        return mMaxFileCount;
    }

    public String getFilePrefix() {
        return mFilePrefix;
    }

    public boolean isGzEnable() {
        return gzEnable;
    }
}