package cn.wangzq.flog.appender;

import android.text.TextUtils;
import android.util.Log;

import java.util.Properties;


public class LogcatAppender extends BaseAppender {

    private static final String KEY_TAG = "Tag";

    private String mTag = null;


    @Override
    public void init(Properties p) {
        super.init(p);
        this.mTag = p.getProperty(KEY_TAG, this.mTag);
    }

    private String getTag(String tag) {
        if (TextUtils.isEmpty(mTag)) {
            return tag;
        }
        return mTag;
    }

    private String getMsg(String tag, String msg) {
        StackTraceElement[] stack = new Throwable().getStackTrace();
        String sourceInfo = stack[6].getFileName() + ":" + stack[6].getLineNumber();
        if (!TextUtils.isEmpty(mTag)) {
            msg = tag + ": " + msg;
        }

        return msg + " (" + sourceInfo + ")";
    }

    @Override
    public void v(String tag, String msg) {
        Log.v(this.getTag(tag), getMsg(tag, msg));
    }

    @Override
    public void v(String tag, String msg, Throwable e) {
        Log.v(this.getTag(tag), getMsg(tag, msg), e);
    }

    @Override
    public void d(String tag, String msg) {
        Log.d(this.getTag(tag), getMsg(tag, msg));
    }

    @Override
    public void i(String tag, String msg) {
        Log.i(this.getTag(tag), getMsg(tag, msg));
    }

    @Override
    public void w(String tag, String msg) {
        Log.w(this.getTag(tag), getMsg(tag, msg));
    }

    @Override
    public void w(String tag, Throwable throwable) {
        Log.w(this.getTag(tag), throwable);
    }

    @Override
    public void w(String tag, String msg, Throwable throwable) {
        Log.w(this.getTag(tag), getMsg(tag, msg), throwable);
    }

    @Override
    public void e(String tag, String msg) {
        Log.e(this.getTag(tag), getMsg(tag, msg));
    }

    @Override
    public void e(String tag, String msg, Throwable throwable) {
        Log.e(this.getTag(tag), getMsg(tag, msg), throwable);
    }

}
