package cn.wangzq.flog.appender;

import android.annotation.SuppressLint;
import android.system.Os;
import android.text.TextUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import cn.wangzq.flog.appender.cache.LFUCache;
import cn.wangzq.flog.appender.filter.ILogFilter;
import cn.wangzq.flog.appender.filter.LogMsgFilter;
import cn.wangzq.flog.common.secret.DefaultKeyGenerate;
import cn.wangzq.flog.common.secret.KeyGenerate;
import cn.wangzq.flog.common.secret.SecretUtils;

public abstract class BaseFileAppender extends BaseAppender {

    protected static final String KEY_FILTER_ENABLE = "FilterMessage";
    protected static final String KEY_MAX_FILE_SIZE = "MaxFileSize";
    protected static final String KEY_LOG_PATH = "Path";
    protected static final String KEY_SECRET = "Secret";
    protected static final String KEY_SECRET_KEY_GENERATE = "KeyGenerate";
    private final ILogFilter mFilter = new LogMsgFilter(new LFUCache<>(10000), 128);
    @SuppressLint("SimpleDateFormat")
    private final DateFormat mTimeFormat = new SimpleDateFormat("Z yyyy-MM-dd HH:mm:ss.SSS");


    private final ExecutorService mWriteThread = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), new ThreadFactory() {
        private final AtomicInteger threadNumber = new AtomicInteger(1);

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(null, r, "FLOG_WRITE:" + threadNumber.getAndIncrement(), 0);
            if (t.isDaemon()) t.setDaemon(false);
            if (t.getPriority() != Thread.NORM_PRIORITY) t.setPriority(Thread.NORM_PRIORITY);
            return t;
        }
    });
    private boolean filterMsg = false;
    private long mMaxFileSize = 1024L * 1024L * 5L;
    private String mLogPath;

    private boolean secret = false;

    private SecretUtils mSecretUtils;


    @Override
    public void init(Properties p) {
        super.init(p);
        mMaxFileSize = Long.parseLong(p.getProperty(KEY_MAX_FILE_SIZE, mMaxFileSize + ""));
        mLogPath = p.getProperty(KEY_LOG_PATH);
        if (null == mLogPath || TextUtils.isEmpty(mLogPath)) {
            throw new IllegalArgumentException("config error! not find Path !");
        }
        filterMsg = Boolean.parseBoolean(p.getProperty(KEY_FILTER_ENABLE, "false"));
        secret = Boolean.parseBoolean(p.getProperty(KEY_SECRET, "false"));
        if (secret) {
            String clazz = p.getProperty(KEY_SECRET_KEY_GENERATE);
            try {
                KeyGenerate keyGenerate;
                // 如果没有配置key 使用默认实现
                if (null == clazz || TextUtils.isEmpty(clazz)) {
                    keyGenerate = new DefaultKeyGenerate();
                } else {
                    keyGenerate = (KeyGenerate) Class.forName(clazz).newInstance();
                }
                mSecretUtils = new SecretUtils(keyGenerate);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void v(String tag, String msg) {
        sendWriteMessage("V", tag, msg);
    }

    @Override
    public void v(String tag, String msg, Throwable e) {
        sendWriteMessage("V", tag, msg + "\n" + getStackTrace(e));
    }

    @Override
    public void d(String tag, String msg) {
        sendWriteMessage("D", tag, msg);

    }

    @Override
    public void i(String tag, String msg) {
        sendWriteMessage("I", tag, msg);

    }

    @Override
    public void w(String tag, String msg) {
        sendWriteMessage("W", tag, msg);

    }

    @Override
    public void w(String tag, Throwable throwable) {
        sendWriteMessage("W", tag, getStackTrace(throwable));

    }

    @Override
    public void w(String tag, String msg, Throwable throwable) {
        sendWriteMessage("W", tag, msg + "\n" + getStackTrace(throwable));
    }

    @Override
    public void e(String tag, String msg) {
        sendWriteMessage("E", tag, msg);
    }

    @Override
    public void e(String tag, String msg, Throwable throwable) {
        sendWriteMessage("E", tag, msg + "\n" + getStackTrace(throwable));
    }

    private static String getStackTrace(Throwable tr) {
        if (tr == null) {
            return "";
        }
        Throwable t = tr;
        int count = 4;
        while (t != null && count > 0) {
            t = t.getCause();
            count--;
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }


    private String getSourceInfo() {
        StackTraceElement[] stacks = new Throwable().getStackTrace();
        return stacks[7].getFileName() + ":" + stacks[7].getLineNumber();
    }

    private void sendWriteMessage(String level, String tag, String msg) {
        String time = mTimeFormat.format(Calendar.getInstance().getTime());
        int pid = Os.getpid();
        int tid = Os.gettid();
        String source = getSourceInfo();
        if (isFilterMsg()) {
            msg = mFilter.getFilterMsg(level, tag, msg);
        }

        String str = String.format(Locale.getDefault(), "[%s] %s [%d-%d] %s: %s @ %s %n", level, time, pid, tid, tag, msg, source);

        mWriteThread.execute(() -> {
            byte[] data = str.getBytes(StandardCharsets.UTF_8);
            if (isSecret()) {
                // 加密数据
                data = mSecretUtils.encrypt(data);
                // 添加加密数据协议头
                data = warpEncryptData(data);

            }
            writeToFile(data);
        });

    }

    private byte[] warpEncryptData(byte[] data) {

        int headSize = 12;
        byte[] encryptData = new byte[headSize + data.length];
        encryptData[0] = 0x1E;
        encryptData[1] = 0x1F;

        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(data.length);
        buffer.flip();
        byte[] size = buffer.array();
        System.arraycopy(size, 0, encryptData, 2, size.length);

        encryptData[10] = 0x1F;
        encryptData[11] = 0x1E;

        System.arraycopy(data, 0, encryptData, headSize, data.length);

        return encryptData;
    }

    abstract void writeToFile(byte[] data);


    public boolean isFilterMsg() {
        return filterMsg;
    }

    public String getLogPath() {
        return mLogPath;
    }

    public long getMaxFileSize() {
        return mMaxFileSize;
    }

    public boolean isSecret() {
        return secret;
    }
}
