package cn.wangzq.flog.appender.filter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cn.wangzq.flog.appender.cache.ICache;

public class LogMsgFilter implements ILogFilter {

    private final ICache<String> mCache;
    private final int msgLength;

    public LogMsgFilter(ICache<String> mCache, int msgLength) {
        this.mCache = mCache;
        this.msgLength = msgLength;
    }

    private String toHexString(byte[] bytes) {

        StringBuilder hex = new StringBuilder(bytes.length * 2);
        for (byte aByte : bytes) {
            hex.append(Character.forDigit((aByte & 0XF0) >> 4, 16));
            hex.append(Character.forDigit((aByte & 0X0F), 16));
        }
        return hex.toString();
    }

    private String getMessageDigest(String data) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert messageDigest != null;
        byte[] digest = messageDigest.digest(data.getBytes());
        return toHexString(digest);
    }

    public String getFilterMsg(String level, String tag, String msg) {

        String data = level + "-" + tag + "-" + msg;
        // 计算组合的消息摘要,并且作为缓存的key
        String digest = getMessageDigest(data);
        // 查询缓存
        String cacheMsg = mCache.get(digest);
        if (null != cacheMsg) {
            // 有缓存,直接用缓存的数据
            return cacheMsg;
        }
        // 没有缓存
        //  判断数据大于 一定值,做消息精简压缩
        String sourceMsg = msg;
        if (msg.length() > msgLength) {
            // 截取msg
            msg = msg.substring(0, 60 - digest.length() - 1);
        }
        // 将msg 拼接后缓存
        cacheMsg = msg + ":" + digest;
        mCache.put(digest, cacheMsg);
        // 第一次的msg 不压缩,便于消息维护,所以直接返回
        return sourceMsg + ":" + digest;
    }
}
