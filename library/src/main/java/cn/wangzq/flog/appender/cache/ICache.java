package cn.wangzq.flog.appender.cache;

public interface ICache<T> {

    T get(String key);

    void put(String key, T val);

    void clean();

}
