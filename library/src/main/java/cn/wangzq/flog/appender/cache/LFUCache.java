package cn.wangzq.flog.appender.cache;


import android.os.SystemClock;

import java.util.Collections;
import java.util.HashMap;

public class LFUCache<T> implements ICache<T> {

    private static final String TAG = "LFUCache";
    private final HashMap<String, LFUCounter> mCacheCounter = new HashMap<>();
    private final HashMap<String, T> mCachedData = new HashMap<>();
    private int maxCacheSize = 0;

    public LFUCache(int cacheSize) {
        maxCacheSize = cacheSize;
    }

    @Override
    public T get(String key) {
        T data = mCachedData.get(key);
        if (null != data) {
            updateCounter(key);
        }
//        Log.w(TAG,"get:"+key +" empty:"+(null == data));
        return data;
    }

    @Override
    public void put(String key, T val) {


        if (mCacheCounter.size() >= maxCacheSize) {
            removeElement();
        }

        LFUCounter counter = mCacheCounter.get(key);
        if (null == counter) {
            long time = SystemClock.elapsedRealtime();
            counter = new LFUCounter(key, 1, time);
            mCacheCounter.put(key, counter);
        }
//        Log.w(TAG,"put:"+key);
        mCachedData.put(key, val);
    }

    @Override
    public void clean() {
        mCacheCounter.clear();
        mCachedData.clear();
    }


    private void updateCounter(String key) {
        LFUCounter counter = mCacheCounter.get(key);
        if (null != counter) {
            long cur = counter.counter;
            if (cur <= Long.MAX_VALUE - 2) {
                cur = cur + 1;
            }
            counter.setCounter(cur);
            long time = SystemClock.elapsedRealtime();
            counter.setLastTime(time);
        }
//        Log.i(TAG, "updateCounter: "+key+" "+counter.counter+" "  +counter.lastTime);
    }

    private synchronized void removeElement() {
        LFUCounter last = Collections.min(mCacheCounter.values());
        mCacheCounter.remove(last.key);
        mCachedData.remove(last.key);
//        Log.w(TAG,"removeElement: "+last.key+" "+last.counter+" "+last.lastTime);
    }

    static class LFUCounter implements Comparable<LFUCounter> {
        String key;
        long counter;
        long lastTime;

        public LFUCounter(String key, long counter, long lastTime) {
            this.key = key;
            this.counter = counter;
            this.lastTime = lastTime;
        }

        public void setCounter(long counter) {
            this.counter = counter;
        }

        public void setLastTime(long lastTime) {
            this.lastTime = lastTime;
        }

        @Override
        public int compareTo(LFUCounter o) {
            int res = Long.compare(this.counter, o.counter);
            return res == 0 ? Long.compare(this.lastTime, o.lastTime) : res;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LFUCounter counter1 = (LFUCounter) o;

            if (counter != counter1.counter) return false;
            if (lastTime != counter1.lastTime) return false;
            return key.equals(counter1.key);
        }

        @Override
        public int hashCode() {
            int result = key.hashCode();
            result = 31 * result + (int) (counter ^ (counter >>> 32));
            result = 31 * result + (int) (lastTime ^ (lastTime >>> 32));
            return result;
        }
    }
}
