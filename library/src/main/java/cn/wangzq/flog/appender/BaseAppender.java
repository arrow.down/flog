package cn.wangzq.flog.appender;

import java.util.Properties;

import cn.wangzq.flog.ILog;
import cn.wangzq.flog.Level;

public abstract class BaseAppender implements ILog {
    public static final String KEY_LOG_LEVEL = "Level";

    private int level = Level.NONE;

    public int getLevel() {
        return this.level;
    }

    @Override
    public boolean isEnable(int level) {
        return level >= getLevel();
    }

    @Override
    public void init(Properties p) {
        level = Level.parse(p.getProperty(KEY_LOG_LEVEL, "V"));
    }
}
