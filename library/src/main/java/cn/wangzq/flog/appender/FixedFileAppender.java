package cn.wangzq.flog.appender;


import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;


public class FixedFileAppender extends BaseFileAppender {


    //协议内容格式

    /*
    0-7     | 固定协议头 FLOG 预留 4 字节
    8-15    | 最后一次 log 记录的偏移量 lastPos
    16-23   | log 循环次数
    24-31   | log 循环最后一次结束位置
    126-127 | \r\n 换行 协议结束
     */
    private static final byte[] PROTOCOL_HEAD = "FLOG".getBytes(StandardCharsets.UTF_8);
    private static final byte[] PROTOCOL_END = "\r\n".getBytes();

    public static final int PROTOCOL_LENGTH = 128;
    public static final int INDEX_HEAD = 0;
    public static final int HEAD_LENGTH = 8;
    public static final int INDEX_LAST_POS = INDEX_HEAD + HEAD_LENGTH;
    public static final int LAST_POS_LENGTH = 8;
    public static final int INDEX_CYCLE = INDEX_LAST_POS + LAST_POS_LENGTH;
    public static final int CYCLE_LENGTH = 8;

    public static final int INDEX_LAST_CYCLE_END = INDEX_CYCLE + CYCLE_LENGTH;

    public static final int Last_CYCLE_END_LENGTH = 8;
    public static final int PROTOCOL_END_LENGTH = PROTOCOL_END.length;
    public static final int INDEX_PROTOCOL_END = PROTOCOL_LENGTH - PROTOCOL_END_LENGTH;

    private RandomAccessFile mLogFile;
    private FileChannel mChannel;


    private long getLastPos() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        mChannel.position(INDEX_LAST_POS).read(buffer);
        buffer.flip();
        return buffer.getLong();
    }

    private void updateLastPos(long pos) throws IOException {
        mChannel.position(INDEX_LAST_POS);
        ByteBuffer b = ByteBuffer.allocate(Long.BYTES);
        b.putLong(pos);
        b.flip();
        mChannel.write(b);
    }

    private void updateLastCycleEndPos(long pos) throws IOException {
        mChannel.position(INDEX_LAST_CYCLE_END);
        ByteBuffer b = ByteBuffer.allocate(Long.BYTES);
        b.putLong(pos);
        b.flip();
        mChannel.write(b);
    }


    private long getCycle() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        mChannel.position(INDEX_CYCLE).read(buffer);
        buffer.flip();
        return buffer.getLong();
    }

    private void updateCycle(long cycle) throws IOException {
        mChannel.position(INDEX_CYCLE);
        ByteBuffer b = ByteBuffer.allocate(Long.BYTES);
        b.putLong(cycle);
        b.flip();
        mChannel.write(b);
    }

    private synchronized void lazyInit() throws IOException {
        if (null == mLogFile) {
            File file = new File(getLogPath());
            boolean isFileExists = file.exists();

            mLogFile = new RandomAccessFile(file, "rw");
            mLogFile.setLength(getMaxFileSize());
            mChannel = mLogFile.getChannel();

            //文件不存在.创建文件并初始化协议
            if (!isFileExists) {
                mChannel.position(INDEX_HEAD);
                mChannel.write(ByteBuffer.wrap(PROTOCOL_HEAD));
                updateLastPos(PROTOCOL_LENGTH);
                updateCycle(0);
                updateLastCycleEndPos(PROTOCOL_LENGTH);
                //如果是新文件,第一次写入文件协议头内容
                mChannel.position(INDEX_PROTOCOL_END);
                mChannel.write(ByteBuffer.wrap(PROTOCOL_END));
                mLogFile.getFD().sync();
            }
        }
    }

    @Override
    void writeToFile(byte[] msg) {
        try {
            lazyInit();

            // 获取记录的lastPos作为本次log 的起始位置
            long startWritePos = getLastPos();
            long msgLength = msg.length;

            long lastPos = msgLength + startWritePos;
            if (lastPos >= getMaxFileSize()) {
                // 从头开始表示循环+1 记录到log 中
                long cycle = getCycle() + 1;
                updateCycle(cycle);
                // 记录上次循环结束位置,作为循环后解密的结束位置
                updateLastCycleEndPos(startWritePos);
                // 写入数据如果要超过 mFileSize 需要从头开始写
                startWritePos = PROTOCOL_LENGTH;
            }
            mChannel.position(startWritePos);
            ByteBuffer writeBuffer = ByteBuffer.wrap(msg);
            // 写入log
            mChannel.write(writeBuffer);
            // 获取最后写入的位置
            lastPos = mChannel.position();
            //写入 lastPos
            updateLastPos(lastPos);

            mLogFile.getFD().sync();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
