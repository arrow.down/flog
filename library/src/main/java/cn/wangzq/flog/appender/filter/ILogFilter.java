package cn.wangzq.flog.appender.filter;

public interface ILogFilter {
    /**
     * @param level log level
     * @param tag   log tag
     * @param msg   log msg
     * @return new msg
     */
    String getFilterMsg(String level, String tag, String msg);
}
