package cn.wangzq.flog;

import java.util.Locale;

public final class Level {
    public static final int VERBOSE = 2;
    public static final int DEBUG = 3;
    public static final int INFO = 4;
    public static final int WARN = 5;
    public static final int ERROR = 6;
    public static final int NONE = 999;

    private Level() {
    }


    public static int parse(String level) {

        int res;
        level = level.toUpperCase(Locale.getDefault());
        switch (level) {
            case "V":
            case "VERBOSE":
                res = VERBOSE;
                break;
            case "D":
            case "DEBUG":
                res = DEBUG;
                break;
            case "I":
            case "INFO":
                res = INFO;
                break;
            case "W":
            case "WARN":
                res = WARN;
                break;
            case "E":
            case "ERROR":
                res = ERROR;
                break;
            case "NONE":
            default:
                res = NONE;
                break;
        }
        return res;
    }
}
