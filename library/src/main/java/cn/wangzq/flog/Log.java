package cn.wangzq.flog;


import java.util.HashMap;
import java.util.Properties;


public class Log {

    private Log() {
    }

    private static final HashMap<String, ILog> sLogger = new HashMap<>();

    public static void init(Properties config) throws Exception {

        /*
        1. 解析配置文件
        2. 解析添加的appender
        3. 为appender 加载配置
         */
        String logger = config.getProperty("flog.Logger", null);
        if (null == logger) {
            throw new IllegalArgumentException("config error ! not find flog.Logger !");
        }
        String[] loggers = logger.split(",");

        String level = config.getProperty("flog.Level", "I");
        for (String log : loggers) {
            String keyAppender = "flog.appender." + log;
            Properties appenderConf = new Properties();
            appenderConf.put("Level", level);
            config.keySet()
                    .stream()
                    .filter((k) -> ((String) k).startsWith(keyAppender) && !keyAppender.equals(k))
                    .forEach((key) -> {
                        String val = config.getProperty((String) key);
                        String appenderConfKey = ((String) key).substring(keyAppender.length() + 1);
                        appenderConf.put(appenderConfKey, val);
                    });

            String appenderClass = config.getProperty(keyAppender);

            Class<ILog> clazz = (Class<ILog>) Class.forName(appenderClass);
            ILog appender = clazz.getDeclaredConstructor().newInstance();
            appender.init(appenderConf);
            sLogger.putIfAbsent(keyAppender, appender);
        }
    }

    public static void v(String tag, String msg) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.VERBOSE)) {
                logger.v(tag, msg);
            }
        });
    }

    public static void v(String tag, String msg, Throwable e) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.VERBOSE)) {
                logger.v(tag, msg, e);
            }
        });
    }

    public static void d(String tag, String msg) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.DEBUG)) {
                logger.d(tag, msg);
            }
        });
    }

    public static void i(String tag, String msg) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.INFO)) {
                logger.i(tag, msg);
            }
        });
    }

    public static void w(String tag, String msg) {

        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.WARN)) {
                logger.w(tag, msg);
            }
        });
    }

    public static void w(String tag, Throwable throwable) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.WARN)) {
                logger.w(tag, throwable);
            }
        });
    }

    public static void w(String tag, String msg, Throwable throwable) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.WARN)) {
                logger.w(tag, msg, throwable);
            }
        });
    }

    public static void e(String tag, String msg) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.ERROR)) {
                logger.e(tag, msg);
            }
        });
    }

    public static void e(String tag, String msg, Throwable throwable) {
        sLogger.forEach((s, logger) -> {
            if (logger.isEnable(Level.ERROR)) {
                logger.e(tag, msg, throwable);
            }
        });
    }
}