package cn.wangzq.flog;

import java.util.Properties;

public interface ILog {


    void init(Properties p);

    void v(String tag, String msg);

    void v(String tag, String msg, Throwable e);

    void d(String tag, String msg);

    void i(String tag, String msg);

    void w(String tag, String msg);

    void w(String tag, Throwable throwable);

    void w(String tag, String msg, Throwable throwable);

    void e(String tag, String msg);

    void e(String tag, String msg, Throwable throwable);

    default boolean isEnable(int level) {
        return true;
    }
}
