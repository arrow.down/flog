package cn.wangzq.flog.tool;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

import cn.wangzq.flog.common.secret.SecretUtils;

@ShellComponent
public class DecryptLogFile {

    Logger mLogger = LoggerFactory.getLogger(DecryptLogFile.class);

    @ShellMethod(value = "加密日志文件解密工具", group = "default")
    public void decrypt(@ShellOption(help = "加密的日志文件路径") String input,
                        @ShellOption(help = "解密后的文件路径") String output,
                        @ShellOption(help = "密码", defaultValue = "FLOG") String key)
            throws IOException {

        SecretUtils utils = new SecretUtils(() -> key);

        File logDecrypt = new File(output);
        File logEncrypt = new File(input);
        if (logDecrypt.exists()) {
            logDecrypt.delete();
        }
        logDecrypt.createNewFile();

        try (FileOutputStream out = new FileOutputStream(logDecrypt, true);
             RandomAccessFile in = new RandomAccessFile(logEncrypt, "r");
        ) {

            FileChannel channel = in.getChannel();

            byte[] protocolBegin = new byte[]{0x1E, 0x1F};
            byte[] protocolEnd = new byte[]{0x1F, 0x1E};

            ByteBuffer protocolHead = ByteBuffer.allocate(2);
            ByteBuffer encryptDataSize = ByteBuffer.allocate(Long.BYTES);

            channel.position(0);

            while (true) {
                channel.read(protocolHead);
                mLogger.debug("READ: 0x{}{} ", Integer.toString(Byte.toUnsignedInt(protocolHead.get(0)), 16), Integer.toString(Byte.toUnsignedInt(protocolHead.get(1)), 16));
                long beginPos = channel.position();
                if (Arrays.compare(protocolBegin, protocolHead.array()) == 0) {

                    long endPos = beginPos + Long.BYTES;
                    mLogger.debug("find begin 1e1e ! {}", beginPos);
                    channel.position(endPos);
                    protocolHead.clear();
                    channel.read(protocolHead);
                    long encryptDataPos = channel.position();

                    if (Arrays.compare(protocolEnd, protocolHead.array()) == 0) {
                        mLogger.debug("find end 1f1e ! {}", encryptDataPos);
                        channel.position(beginPos);
                        channel.read(encryptDataSize);
                        encryptDataSize.flip();
                        long size = encryptDataSize.getLong();
                        mLogger.info("find encrypt data size {}", size);
                        channel.position(encryptDataPos);
                        ByteBuffer enc = ByteBuffer.allocate((int) size);
                        channel.read(enc);
                        enc.flip();
                        out.write(utils.decrypt(enc.array()));
                    }
                    protocolHead.clear();
                    encryptDataSize.clear();
                    continue;
                }
                if (beginPos >= in.length()) break;

                out.write(protocolHead.array());
                protocolHead.clear();
            }
        }
        mLogger.info("decrypt finish!");
    }


}
