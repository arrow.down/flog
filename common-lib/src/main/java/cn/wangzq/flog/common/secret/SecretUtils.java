package cn.wangzq.flog.common.secret;



import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class SecretUtils {

    private final KeyGenerate generate;

    private final String transformation = "AES/ECB/PKCS5Padding";

    public SecretUtils(KeyGenerate generate) {
        this.generate = generate;
    }

    private Key getSecretKey() {
        String key = generate.getKey();
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        messageDigest.update(key.getBytes());
        byte[] digest = messageDigest.digest();
        return new SecretKeySpec(digest, "AES");
    }

    public byte[] encrypt(byte[] msg) {
        try {
            Cipher cipher = Cipher.getInstance(transformation);
            Key secretKey = getSecretKey();
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(msg);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException |
                 BadPaddingException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] decrypt(byte[] msg) {
        try {
            Cipher cipher = Cipher.getInstance(transformation);
            Key secretKey = getSecretKey();
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return cipher.doFinal(msg);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException |
                 BadPaddingException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }


}
