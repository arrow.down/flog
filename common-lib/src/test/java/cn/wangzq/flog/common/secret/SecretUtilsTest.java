package cn.wangzq.flog.common.secret;

import junit.framework.TestCase;


import org.junit.Assert;

import java.util.Arrays;
import java.util.HashMap;


public class SecretUtilsTest extends TestCase {

    SecretUtils secret = new SecretUtils(() -> "xx");

    static HashMap<byte[], byte[]> data = new HashMap<>();

    static {
        data.put(new byte[]{49, 45, 45, 60, 49, 54},
                new byte[]{39, -34, -13, 87, -21, 32, 124, -41, 100, 109, -60, 80, 24, -124, -33, -19});

        data.put(new byte[]{120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 61, 49, 54},
                new byte[]{40, 124, 116, -30, -71, 19, 36, -72, 122, 16, -61, -78, 35, -48, -111, 46, 26, 63, 31, 59, 33, -46, -44, -122, 119, 122, -49, -87, 40, 68, -46, -79});

        data.put(new byte[]{88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 62, 49, 54, 60, 51, 50},
                new byte[]{17, 21, -84, 76, -17, -100, 85, 80, 16, 57, 74, -120, 18, -55, -16, 116, -67, -116, -50, 122, 26, -95, 46, 96, -66, -82, 110, 87, 2, -60, -127, -10});


        data.put(new byte[]{120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 61, 51, 50, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 61, 51, 50},
                new byte[]{-53, 7, 7, 1, 51, -83, -56, 124, -89, -81, 34, 88, 53, 49, 50, 58, -53, 7, 7, 1, 51, -83, -56, 124, -89, -81, 34, 88, 53, 49, 50, 58, 26, 63, 31, 59, 33, -46, -44, -122, 119, 122, -49, -87, 40, 68, -46, -79});

        data.put(new byte[]{120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 61, 51, 50, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 61, 62, 62, 51, 50, 62, 51, 50, 60, 52, 56},
                new byte[]{-53, 7, 7, 1, 51, -83, -56, 124, -89, -81, 34, 88, 53, 49, 50, 58, 8, 80, -111, -96, -53, -3, 76, 73, 112, -108, 123, 120, 94, 10, 90, 94, -126, 120, 107, 106, -80, 74, 37, -95, -112, -71, -119, 9, 58, 56, 118, -106});

    }


    public void testEncrypt() {

        data.forEach((source,target)->{
            byte[] encrypt = secret.encrypt(source);
            System.out.println("---------------------------------------------------------");
            System.out.println("source:" + source.length + " " + Arrays.toString(source));
            System.out.println("target:" + target.length + " " + Arrays.toString(target));
            System.out.println("encrypt:" + encrypt.length + " " + Arrays.toString(encrypt));
            Assert.assertArrayEquals(target,encrypt);
        });
    }

    public void testDecrypt() {
        data.forEach((source,target)->{
            byte[] decrypt = secret.decrypt(target);
            System.out.println("---------------------------------------------------------");
            System.out.println("source:" + source.length + " " + Arrays.toString(source));
            System.out.println("target:" + target.length + " " + Arrays.toString(target));
            System.out.println("decrypt:" + decrypt.length + " " + Arrays.toString(decrypt));
            Assert.assertArrayEquals(source,decrypt);
        });


    }
}