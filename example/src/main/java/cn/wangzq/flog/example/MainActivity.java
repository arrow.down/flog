package cn.wangzq.flog.example;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.wangzq.flog.Log;

public class MainActivity extends AppCompatActivity {


    private String TAG = "MainActivity";
    private IFlogService mService;

    private ServiceConnection mRemoteConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IFlogService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    ExecutorService service = Executors.newFixedThreadPool(10);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions(new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, 10);

        bindService(new Intent(this, FLogRemoteService.class), mRemoteConnection, BIND_AUTO_CREATE);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this::onViewClick);
    }

    public void onViewClick(View view) {
        String k = "KK";//UUID.randomUUID().toString();
        int i = 0;
        while (i++ < 10) {
            String msg = i + " : " + k + " AAAAAAAA BBBBBBBBB CCCCCCC DDDDDDDD EEEEEEEE FFFFFFFF  END";
            service.submit(() -> {
                try {
                    Log.v(TAG, msg);
                    Log.v(TAG, msg, new Throwable("vvvv"));
                    Log.d(TAG, msg);
                    Log.i(TAG, msg);
                    Log.w(TAG, msg);
                    Log.w(TAG, new Throwable("wwww"));
                    Log.w(TAG, msg, new Throwable("wwww2"));
                    Log.e(TAG, msg);
                    Log.e(TAG, msg, new Throwable("EEEE"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    mService.writeLog(msg);
                } catch (RemoteException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            });

        }
    }

    @Override
    protected void onDestroy() {
        unbindService(mRemoteConnection);
        super.onDestroy();
    }
}
