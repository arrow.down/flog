package cn.wangzq.flog.example.utils;

import android.annotation.SuppressLint;

import java.lang.reflect.Method;

@SuppressLint("PrivateApi")
public class SystemPropertiesUtil {
    public static String get(String key) {
        String value = "";
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            value = (String) (get.invoke(null, key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String get(String key, String defaultValue) {
        String value = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);
            value = (String) (get.invoke(null, key, defaultValue));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;

    }

    public static int getInt(String key, int defaultValue) {
        int value = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("getInt", String.class, int.class);
            value = (Integer) (get.invoke(null, key, defaultValue));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static long getLong(String key, long defaultValue) {
        long value = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("getLong", String.class, long.class);
            value = (Long) (get.invoke(null, key, defaultValue));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;

    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("getBoolean", String.class, boolean.class);
            value = (Boolean) (get.invoke(null, key, defaultValue));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;

    }

    public static void set(String key, String value) {
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method set = c.getMethod("set", String.class, String.class);
            set.invoke(null, key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}