package cn.wangzq.flog.example;

import android.app.Application;

import java.io.InputStream;
import java.util.Properties;

import cn.wangzq.flog.Log;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        getCacheDir();
        Properties properties = new Properties();
        try (InputStream stream = getAssets().open("flog/config.properties")) {
            properties.load(stream);
            Log.init(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
