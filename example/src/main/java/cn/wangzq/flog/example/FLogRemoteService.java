package cn.wangzq.flog.example;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import cn.wangzq.flog.Log;

public class FLogRemoteService extends Service {

    private String TAG = "FLogRemoteService";

    public FLogRemoteService() {
    }

    private FlogService mService;

    @Override
    public IBinder onBind(Intent intent) {
        if (null == mService) {
            mService = new FlogService();
        }
        return mService;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class FlogService extends IFlogService.Stub {
        @Override
        public void writeLog(String s) {
            s = "REMOTE:"+s;
            Log.v(TAG, s );
            Log.v(TAG,s,new Throwable("vvvv"));
            Log.d(TAG, s );
            Log.i(TAG, s );
            Log.w(TAG, s );
            Log.w(TAG,new Throwable("wwww"));
            Log.w(TAG,s,new Throwable("wwww2"));
            Log.e(TAG, s );
            Log.e(TAG,s,new Throwable("EEEE"));
        }
    }

}
